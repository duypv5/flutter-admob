import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:test2_admod/banner/ad_help.dart';

class RewardAdPage extends StatefulWidget {
  const RewardAdPage({Key? key}) : super(key: key);

  @override
  State<RewardAdPage> createState() => _RewardAdPageState();
}

const int maxFailedLoadAttempts = 3;

class _RewardAdPageState extends State<RewardAdPage> {
  List<String> entries = ["a", "b", "c", "d", "e", "f", "g", "h", "i"];
  int _interstitialLoadAttempts = 0;
  RewardedAd? _rewardedAd;
  void createRewardAd() {
    RewardedAd.load(
        adUnitId: AdHelper.nativeAdUnitId,
        request: AdRequest(),
        rewardedAdLoadCallback: RewardedAdLoadCallback(
          onAdLoaded: (RewardedAd ad) {
            print('$ad loaded.');
            // Keep a reference to the ad so you can show it later.
            this._rewardedAd = ad;
          },
          onAdFailedToLoad: (LoadAdError error) {
            _interstitialLoadAttempts += 1;
            _rewardedAd = null;
            if (_interstitialLoadAttempts <= maxFailedLoadAttempts) {
              createRewardAd();
            }
          },
        ));
  }

  void showRewaedAd() {
    if (_rewardedAd != null) {
      _rewardedAd!.fullScreenContentCallback = FullScreenContentCallback(
        onAdShowedFullScreenContent: (RewardedAd ad) =>
            print('$ad onAdShowedFullScreenContent.'),
        onAdDismissedFullScreenContent: (RewardedAd ad) {
          print('$ad onAdDismissedFullScreenContent.');
          ad.dispose();
          createRewardAd();
        },
        onAdFailedToShowFullScreenContent: (RewardedAd ad, AdError error) {
          print('$ad onAdFailedToShowFullScreenContent: $error');
          ad.dispose();
          createRewardAd();
        },
        onAdImpression: (RewardedAd ad) => print('$ad impression occurred.'),
      );
    }
   // OnUserEarnedRewardCallback callback;
    _rewardedAd!.show(
        onUserEarnedReward: (AdWithoutView ad, RewardItem reward) {
          print('$ad with reward $RewardItem(${reward.amount}, ${reward.type})');
        });
  }

  @override
  void dispose() {
    _rewardedAd!.dispose();
    super.dispose();
  }

  @override
  void initState() {
    createRewardAd();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
          shrinkWrap: true,
          itemCount: entries.length,
          itemBuilder: (context, index) {
            return GestureDetector(
                onTap: () {
                  print("duy");
                  showRewaedAd();
                },
                child: ListTile(
                  leading: const Icon(
                    Icons.person,
                    size: 22,
                  ),
                  title: Text(entries[index].toString()),
                  trailing: const Icon(
                    Icons.arrow_forward_ios,
                    size: 22,
                  ),
                ));
          }),
    );
  }
}
