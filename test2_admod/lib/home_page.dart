import 'package:flutter/material.dart';
import 'package:test2_admod/ad_open_app/app_open.dart';
import 'package:test2_admod/banner/banner_page.dart';
import 'package:test2_admod/native_ad/navite_ad_page.dart';
import 'package:test2_admod/reward_ad/reward_ap_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({ Key? key }) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return  DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar( 
          bottom:const TabBar(
            isScrollable: true,
            indicatorColor: Colors.deepOrange,
            tabs: [
              Tab(text: 'Ad open app',),
              Tab(text: 'ad banner',),            
              Tab(text: 'ad reward',),
              Tab(text: 'ad navite',),
            ],
          ),
          title:const  Text('Flutter admob'),
          centerTitle:  true,
        ),
        body:const TabBarView(
          children: [
              AppOpenPage(),
              BannerPage(),
              RewardAdPage(),
              NaviteAdPage(),
          ])
      ),
    );
  }
}