
import 'package:flutter/material.dart';
import 'package:test2_admod/ad_open_app/app_lifecycle_reactor.dart';
import 'package:test2_admod/ad_open_app/open_manager.dart';


class AppOpenPage extends StatefulWidget {
  const AppOpenPage({ Key? key }) : super(key: key);

  @override
  State<AppOpenPage> createState() => _AppOpenPageState();
}

class _AppOpenPageState extends State<AppOpenPage> {
  late AppLifecycleReactor _appLifecycleReactor;

  @override
  void initState() {
    super.initState();
    AppOpenAdManager appOpenAdManager = AppOpenAdManager()..loadAd();
    _appLifecycleReactor = AppLifecycleReactor(
    appOpenAdManager: appOpenAdManager);
    _appLifecycleReactor.listenToAppStateChanges();
  }


  @override
  Widget build(BuildContext context) {
     return Scaffold(
       body: Text("ads"),
     );
  }
}