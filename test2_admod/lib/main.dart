
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:test2_admod/home_page.dart';
import 'package:test2_admod/native_ad/navite_ad_page.dart';
import 'package:test2_admod/reward_ad/reward_ap_page.dart';

void main() {
    WidgetsFlutterBinding.ensureInitialized();
    MobileAds.instance.initialize();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App Open Example',
      debugShowCheckedModeBanner:  false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const NaviteAdPage()
    );
  }
}

