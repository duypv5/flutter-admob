
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:test2_admod/banner/ad_help.dart';

class NaviteAdPage extends StatefulWidget {
  const NaviteAdPage({ Key? key }) : super(key: key);

  @override
  State<NaviteAdPage> createState() => _NaviteAdPageState();
}


class _NaviteAdPageState extends State<NaviteAdPage> {
  static const _kAdIndex = 4;
  List<String> entries = ["a", "b", "c", "d", "e", "f","g", "h", "i"];
  late NativeAd _ad;
  //final AdSize adSize=const AdSize(width: 300, height: 50);
  bool _isAdLoaded = false;
  int _getDestinationItemIndex(int rawIndex) {
    if ( (rawIndex >= _kAdIndex && _isAdLoaded)) {
      return rawIndex - 1;
    }
    return rawIndex;
  }
  
  void _createNaviteAd(){
        _ad= NativeAd(
      request: const AdRequest(),
      ///This is a test adUnitId make sure to change it
      adUnitId: AdHelper.nativeAdUnitId,
      factoryId: 'listTile',
      listener: NativeAdListener(
        onAdLoaded: (ad){
          setState(() {
            _isAdLoaded = true;
          });
          print('Ad loaded.');
        },
        onAdOpened: (Ad ad) => print('Ad opened.'),
        // Called when an ad removes an overlay that covers the screen.
        onAdClosed: (Ad ad) => print('Ad closed.'),
        // Called when an impression occurs on the ad.
        onAdImpression: (Ad ad) => print('Ad impression.'),
        // Called when a click is recorded for a NativeAd.
        onNativeAdClicked: (NativeAd ad) => print('Ad clicked.'),
        onAdFailedToLoad: (ad, error){
          ad.dispose();
          print('failed to load the ad ${error.message}, ${error.code}');
        }
      )
    );
      _ad.load(); 
  }
 
  

  @override
  void dispose() {
    _ad.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _createNaviteAd();
  }

  @override
  Widget build(BuildContext context) {
      return Scaffold(
        //  body:ListView.builder(
        //    shrinkWrap: true,
        //    itemCount: entries.length + (_isAdLoaded ? 1 : 0),
        //    itemBuilder: (context, index){
        // if ( _isAdLoaded &&index == _kAdIndex) {
        //       return Container(
        //         child: AdWidget(ad: _ad),
        //         color: Colors.amberAccent,                
        //         alignment: Alignment.center,
        //       );
        //   } else {
        //        final item = entries[_getDestinationItemIndex(index)];
        //       return Padding(
        //         padding: const EdgeInsets.only(
        //           bottom: 10,
        //         ),
        //         child: ListTile(
        //             leading:const  Icon(Icons.person,size: 22,),
        //             title:  Text(item.toString()),
        //             trailing:const Icon(Icons.arrow_forward_ios,size: 22,),
        //         ),
        //       );
        //    }}) ,
        body:Container(
                child: AdWidget(ad: _ad),
                width: 500,
              height: 500,
                alignment: Alignment.center,
              ) ,
      );
    
  }
}