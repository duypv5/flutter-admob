

import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:test2_admod/banner/ad_help.dart';

class InterstitialPage extends StatefulWidget {
  const InterstitialPage({ Key? key }) : super(key: key);

  @override
  State<InterstitialPage> createState() => _InterstitialPageState();
}
 const  int maxFailedLoadAttempts = 3;
class _InterstitialPageState extends State<InterstitialPage> {
  static const _kAdIndex = 5;
  List<String> entries = ["a", "b", "c", "d", "e", "f","g", "h", "i"];
  late BannerAd _ad;
  //final AdSize adSize=const AdSize(width: 300, height: 50);
  bool _isAdLoaded = false;
  int _getDestinationItemIndex(int rawIndex) {
    if ( (rawIndex >= _kAdIndex && _isAdLoaded)) {
      return rawIndex - 1;
    }
    return rawIndex;
  }
  // ad interstitial 
  InterstitialAd? _interstitialAd;
  int _interstitialLoadAttempts = 0;
  void _createInterstitialAd() {
  InterstitialAd.load(
    adUnitId: AdHelper.interstitialAdUnitId,
    request: AdRequest(),
    adLoadCallback: InterstitialAdLoadCallback(
    onAdLoaded: (InterstitialAd ad) {
      _interstitialAd = ad;
      _interstitialLoadAttempts = 0;
    },
    onAdFailedToLoad: (LoadAdError error) {
      _interstitialLoadAttempts += 1;
      _interstitialAd = null;
      if (_interstitialLoadAttempts <= maxFailedLoadAttempts) {
        _createInterstitialAd();
      }
    },
  ));}
  void _showInterstitialAd() {
  if (_interstitialAd != null) {
    _interstitialAd!.fullScreenContentCallback = FullScreenContentCallback(
      onAdDismissedFullScreenContent: (InterstitialAd ad) {
         ad.dispose();
        _createInterstitialAd();
      },
      onAdFailedToShowFullScreenContent: (InterstitialAd ad, AdError error) {
        ad.dispose();
         _createInterstitialAd();
      },
    );
    _interstitialAd!.show();
  }
}
 
  //  ad banner //
  void _createBannerAd(){
     _ad = BannerAd(
      adUnitId: AdHelper.bannerAdUnitId,
      size: AdSize.banner,
      request: const AdRequest(),
      listener: BannerAdListener(
        onAdLoaded: (_) {
          setState(() {
            _isAdLoaded = true;
          });
        },
        onAdFailedToLoad: (ad, error) {
          // Releases an ad resource when it fails to load
          ad.dispose();

          print('Ad load failed (code=${error.code} message=${error.message})');
        },
      ),
    );

    _ad.load();
  }

  @override
  void dispose() {
    _ad.dispose();
    _interstitialAd!.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _createBannerAd(); 
    _createInterstitialAd();
  }

  @override
  Widget build(BuildContext context) {
      return Scaffold(
         body:ListView.builder(
           shrinkWrap: true,
           itemCount: entries.length + (_isAdLoaded ? 1 : 0),
           itemBuilder: (context, index){
        if (_isAdLoaded && index == _kAdIndex) {
            return Container(
              padding:const EdgeInsets.only(
                bottom: 10,
              ),
              width: _ad.size.width.toDouble(),
              height: _ad.size.height.toDouble(),
              child: AdWidget(ad: _ad),
            );
          } else {
               final item = entries[_getDestinationItemIndex(index)];
              return Padding(
                padding: const EdgeInsets.only(
                  bottom: 10,
                ),
                child: GestureDetector(
                  onTap: () {
                    _showInterstitialAd();
                    
                  },
                  child:  ListTile(
                      leading:const  Icon(Icons.person,size: 22,),
                      title:  Text(item.toString()),
                      trailing:const Icon(Icons.arrow_forward_ios,size: 22,),
                  )
                ),
              );
           }}) ,
      );
    
  }
  
}